---
date: 2023-11-27T01:00:00+00:00
authors: 
  - "techknowlogick"
  - "wolfogre"
title: "Gitea Cloud: A brand new platform for managed Gitea Instances"
tags: ["announcement"]
coverImage: /demos/gitea-cloud/cover.png
---

We are delighted to announce that **Gitea Cloud is now officially released!**

Gitea Cloud is a service designed for enterprise organizations to set up and run their own Gitea instances more easily and efficiently.

![Screenshot of gitea cloud instance management dashboard showing details of subscription, and resources used by instance](/demos/gitea-cloud/screenshot.png)

Gitea Cloud is offered by "CommitGo, Inc.", a Delaware company founded by members of Gitea's Technical Oversight Committee to offer services, support, and training to companies and others using Gitea.

### What can Gitea Cloud bring to you?

1. **Simple setup, worry-free maintenance**:

   With just a few clicks, you can easily deploy your own Gitea instance on Gitea Cloud. Our team will handle all the maintenance, including backups, upgrades, and more. This allows you to focus on business development without worrying about operational issues.

2. **Dedicated instances, full control in your hand**:

   By using dedicated infrastructure, you don't have to worry about any noisy neighbors slowing you down. 

   You can use a custom domain, and freely set up organizations and manage permissions according to the structure of your organization.

   Each plan comes with managed CI/CD runners so you can make sure your code is always running smoothly.

   You have the flexibility to choose both the underlying cloud infrastructure provider and the region, ensuring the best option and location for your team.

3. **Flexible pricing, pay-as-you-go**:

   Using a pay-as-you-go model, you can choose a plan that suits your company size and needs, and easily upgrade when demands increase.

   Discounts are available for annual renewal, individuals/small teams, and non-profits.

4. **Efficient, stable, secure and reliable**:

   Gitea Cloud ensures high performance and stability of the service.

   Gitea Cloud is in the process of undergoing SOC2 Type 1 and 2 audits. This ensures your code is kept secure and offers peace of mind to your compliance team.

More details on the service, including an FAQ, can be found at [about.gitea.com](https://about.gitea.com/products/cloud).

Gitea Cloud is currently invite-only, and we will continuously send registration invitations to users on the waitlist.

If you have any questions about Gitea Cloud, or want to skip the waitlist, please reach out to us at support@gitea.com.

Thank you for your continued support!
